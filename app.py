from flask import Flask, request
from dotenv import load_dotenv
from flask_cors import CORS
import os
import ocrOregon
import ocrgeorgia
import splitimage
import ocrlocal
import ocrpg
load_dotenv()

app = Flask(__name__)
app.config["MAX_CONTENT_LENGTH"] = 100 * 1024 * 1024
app.config["UPLOAD_EXTENSIONS"] = [".jpg", ".png", ".pdf"]
# app.config["UPLOAD_FOLDER"] = "./upload"
CORS(app)


@app.route("/ping", methods=["GET"])
def ping():
    return {"statusCode": 200, "body": "success"}


@app.route("/splitimage", methods=["POST"])
def splitImage():
    uploadedFile = request.files["file"]
    # uploadedFile.save("test.pdf")
    splitimage.pdfBinaryImage(uploadedFile.read())
    return {"statusCode": 200, "body": "success"}


@app.route("/categoryimage", methods=["POST"])
def categoryImage():
    requestData = request.get_json()
    print(requestData)
    imageId = requestData["imageId"]
    ocrgeorgia.categoryImage(imageId)
    return {"statusCode": 200, "body": "success"}

@app.route("/categoryimageOCR", methods=["POST"])
def categoryimageOCR():
    requestData = request.get_json()
    print(requestData)
    imageId = requestData["imageId"]
    ocrpg.categoryimageOCR(imageId)
    return {"statusCode": 200, "body": "success"}

@app.route("/localCheck", methods=["GET"])
def localCheck():
    result = ocrlocal.categoryImage()
    return {"statusCode": 200, "body": str(result)}


if __name__ == "__main__":
    PORT = os.environ.get("PORT")
    HOST = os.environ.get("HOST")
    app.run(host=HOST, port=PORT)
