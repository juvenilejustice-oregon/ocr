import boto3
from os import environ
import time

s3BUCKET_NAME = environ.get("s3BUCKET_NAME")
ACCESS_ID = environ.get("ACCESS_ID")
ACCESS_KEY = environ.get("ACCESS_KEY")
AWS_REGION = environ.get("AWS_REGION")

s3client = boto3.client(
    "s3",
    aws_access_key_id=ACCESS_ID,
    aws_secret_access_key=ACCESS_KEY,
    region_name=AWS_REGION,
)

s3resource = boto3.resource(
    "s3",
    aws_access_key_id=ACCESS_ID,
    aws_secret_access_key=ACCESS_KEY,
    region_name=AWS_REGION,
)


def s3upload(binaryData, uploadPath, s3BucketName=s3BUCKET_NAME):
    response = s3client.put_object(
        Body=binaryData,
        Bucket=s3BucketName,
        Key=uploadPath,
    )
    return response


def s3getPdfData(uploadPath, s3BucketName=s3BUCKET_NAME):
    file_byte_string = s3client.get_object(Bucket=s3BucketName, Key=uploadPath)[
        "Body"
    ].read()
    return file_byte_string


def textractAnalyzeDocument(key, featureType, s3BucketName=s3BUCKET_NAME):
    textractClient = boto3.client(
        "textract",
        aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=ACCESS_KEY,
        region_name=AWS_REGION,
    )
    response = textractClient.analyze_document(
        Document={"S3Object": {"Bucket": s3BucketName, "Name": key}},
        FeatureTypes=featureType,
    )
    return response


def startJob(objectName, s3BucketName=s3BUCKET_NAME):
    response = None
    client = boto3.client(
        "textract",
        aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=ACCESS_KEY,
        region_name=AWS_REGION,
    )
    response = client.start_document_text_detection(
        DocumentLocation={"S3Object": {"Bucket": s3BucketName, "Name": objectName}}
    )

    return response["JobId"]


def isJobComplete(jobId):
    time.sleep(5)
    client = boto3.client(
        "textract",
        aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=ACCESS_KEY,
        region_name=AWS_REGION,
    )
    response = client.get_document_text_detection(JobId=jobId)
    status = response["JobStatus"]
    print("Job status: {}".format(status))

    while status == "IN_PROGRESS":
        time.sleep(5)
        response = client.get_document_text_detection(JobId=jobId)
        status = response["JobStatus"]
        print("Job status: {}".format(status))

    return status


def getJobResults(jobId):

    pages = []

    time.sleep(5)

    client = boto3.client(
        "textract",
        aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=ACCESS_KEY,
        region_name=AWS_REGION,
    )
    response = client.get_document_text_detection(JobId=jobId)

    pages.append(response)
    print("Resultset page recieved: {}".format(len(pages)))
    nextToken = None
    if "NextToken" in response:
        nextToken = response["NextToken"]

    while nextToken:
        time.sleep(5)

        response = client.get_document_text_detection(JobId=jobId, NextToken=nextToken)

        pages.append(response)
        print("Resultset page recieved: {}".format(len(pages)))
        nextToken = None
        if "NextToken" in response:
            nextToken = response["NextToken"]

    return pages


def textractAnalyzeDocumentLocal(key, featureType, s3BucketName=s3BUCKET_NAME):
    textractClient = boto3.client(
        "textract",
        aws_access_key_id=ACCESS_ID,
        aws_secret_access_key=ACCESS_KEY,
        region_name=AWS_REGION,
    )
    response = textractClient.analyze_document(
        Document={"Bytes": key},
        FeatureTypes=featureType,
    )
    return response
