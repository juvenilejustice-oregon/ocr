import json
from trp import Document
import csv
import psycopg2
from os import environ
from awsutils import s3resource, s3client, textractAnalyzeDocument

s3BUCKET_NAME = environ.get("s3BUCKET_NAME")
DB_USERNAME = environ.get("DB_USERNAME")
DB_PASSWORD = environ.get("DB_PASSWORD")
DB_HOSTNAME = environ.get("DB_HOSTNAME")
DB_PORT = environ.get("DB_PORT")
DB_DATABASE_NAME = environ.get("DB_DATABASE_NAME")


def categoryImage(imageId):
    # event['Records'][0]['s3']['bucket']['name']
    key = imageId  # event['Records'][0]['s3']['object']['key']
    # keys = []
    # keys.append(keys)

    connection = psycopg2.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOSTNAME,
        port=DB_PORT,
        database=DB_DATABASE_NAME,
    )

    cursor = connection.cursor()

    split_key = key.replace(".jpeg", "")

    x = split_key.split("/")

    documentPathName = x[0] + "/" + x[1]
    documentFileName = x[2]

    # Amazon Textract client

    response = textractAnalyzeDocument(key, ["TABLES"])

    # Get the text blocks
    blocks = response["Blocks"]
    # print(blocks)

    detectedText = ""

    # print detected text
    for item in response["Blocks"]:
        if item["BlockType"] == "LINE":
            detectedText += item["Text"] + "\n"

    genratedrawfile = str(documentPathName) + "/" + str(documentFileName) + str(".txt")
    s3resource.Object(s3BUCKET_NAME, genratedrawfile).put(Body=str(detectedText))

    # print("--------------------------------------------------")
    # print(detectedText)
    # print("--------------------------------------------------")

    blocks_map = {}

    table_blocks = []
    for block in blocks:
        blocks_map[block["Id"]] = block
        if block["BlockType"] == "TABLE":
            table_blocks.append(block)

    if len(table_blocks) >= 0:
        # print('inside the if condition')
        csvcontent = ""
        for index, table in enumerate(table_blocks):
            # print("inside for loop - 1")
            csvcontent += generate_table_csv(table, blocks_map, index + 1)
            # csv += '\n\n'
            genratedfile = (
                str(documentPathName)
                + "/"
                + str(documentFileName)
                + str("_csv_")
                + str(index)
                + str(".csv")
            )
            s3resource.Object(s3BUCKET_NAME, genratedfile).put(Body=csvcontent)
            csvcontent = ""

            # print(genratedfile)
            s3 = s3client
            obj = s3.get_object(Bucket=s3BUCKET_NAME, Key=genratedfile)
            # initial_df = pd.read_csv(obj['Body']) # 'Body' is a key word
            # initial_df = json.dumps(obj['Body'], indent=4)
            initial_df = json.dumps(obj["Body"].read().decode("utf-8"), indent=4)
            json_data = [
                json.dumps(d)
                for d in csv.DictReader(obj["Body"].read().decode("utf-8"))
            ]

            genratedjsonfile = (
                str(documentPathName)
                + "/"
                + str(documentFileName)
                + str("_table_")
                + str(index)
                + str(".json")
            )
            s3resource.Object(s3BUCKET_NAME, genratedjsonfile).put(Body=initial_df)

    catagory_lst = []
    lable_lst = []

    connection = psycopg2.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOSTNAME,
        port=DB_PORT,
        database=DB_DATABASE_NAME,
    )

    cursor = connection.cursor()

    postgreSQL_scannedimage_select_Query = (
        "SELECT id,applicationid, category, imagepath, filename, lable, scanorder, batchno FROM public.scannedimage where documentpagegenid ="
        + documentFileName
        + ";"
    )
    cursor.execute(postgreSQL_scannedimage_select_Query)
    postgreSQL_scannedimage_select_Query_record = cursor.fetchall()

    postgreSQL_ocrdataset_select_Query = "SELECT array_agg(keys) as keys, catagory, active, id, percentage, lable FROM public.ocrdataset where active = true group by catagory, active, id, percentage, lable order by id"
    cursor.execute(postgreSQL_ocrdataset_select_Query)
    postgreSQL_ocrdataset_select_Query_record = cursor.fetchall()

    for row in postgreSQL_scannedimage_select_Query_record:
        # print(row)
        scannedimagelst_id = row[0]
        scannedimagelst_applicationid = row[1]
        scannedimagelst_category = row[2]
        scannedimagelst_imagepath = row[3]
        scannedimagelst_filename = row[4]
        scannedimagelst_lable = row[5]
        scannedimagelst_scanorder = row[6]
        scannedimagelst_batchno = row[7]

        postgreSQL_scannedimagelist_delete_Query = (
            "DELETE FROM public.scannedimagelist WHERE scannedimageuid = '"
            + scannedimagelst_id
            + "'and applicationid = '"
            + scannedimagelst_applicationid
            + "';"
        )
        cursor.execute(postgreSQL_scannedimagelist_delete_Query)
        current_page_catagory_lst = []

        for row in postgreSQL_ocrdataset_select_Query_record:
            # print(row)
            ocrdataset_keys = row[0]
            ocrdataset_catagory = row[1]
            ocrdataset_active = row[2]
            ocrdataset_id = row[3]
            ocrdataset_percentage = row[4]
            ocrdataset_lable = row[5]

            # print(ocrdataset_keys)
            # print(ocrdataset_catagory)
            # print(ocrdataset_active)
            # print(ocrdataset_id)
            # print(ocrdataset_percentage)
            # print(ocrdataset_lable)
            # print("--------------------------------")
            if ocrdataset_catagory not in current_page_catagory_lst:
                status = setCategoryLable(
                    ocrdataset_keys,
                    str(detectedText.lower()),
                    ocrdataset_percentage,
                    scannedimagelst_id,
                    scannedimagelst_applicationid,
                    ocrdataset_catagory,
                    ocrdataset_lable,
                    scannedimagelst_imagepath,
                    scannedimagelst_filename,
                    scannedimagelst_scanorder,
                    scannedimagelst_batchno,
                    ocrdataset_id,
                )

                if status == "Insert":
                    if ocrdataset_id == "tanfdoc":
                        response = textractAnalyzeDocument(key, ["FORMS"])
                        doc = Document(response)
                        for page in doc.pages:
                            for field in page.form.fields:
                                if str(field.key) == "Education":
                                    if str(field.value) == "SELECTED":
                                        insert_table_scannedimagelist(
                                            scannedimagelst_id,
                                            scannedimagelst_applicationid,
                                            "Income",
                                            scannedimagelst_imagepath,
                                            scannedimagelst_filename,
                                            "Income",
                                            scannedimagelst_scanorder,
                                            scannedimagelst_batchno,
                                            ocrdataset_id,
                                        )
                                        catagory_lst.append("Income")
                    current_page_catagory_lst.append(ocrdataset_catagory)
                    if ocrdataset_catagory not in catagory_lst:
                        # res.append(i)
                        catagory_lst.append(ocrdataset_catagory)
                    if ocrdataset_lable not in lable_lst:
                        lable_lst.append(ocrdataset_lable)

        if len(lable_lst) == 0:
            lable_lst.append("Others")

        if len(catagory_lst) == 0:
            catagory_lst.append("Others")
            insert_table_scannedimagelist(
                scannedimagelst_id,
                scannedimagelst_applicationid,
                "Others",
                scannedimagelst_imagepath,
                scannedimagelst_filename,
                "Others",
                scannedimagelst_scanorder,
                scannedimagelst_batchno,
                "Others",
            )

        json_form_data = {}

        if "Application123" in catagory_lst:
            # Call Amazon Textract
            response = textractAnalyzeDocument(key, ["FORMS"])
            doc = Document(response)

            for page in doc.pages:
                for field in page.form.fields:
                    # print("Key: {}, Value: {}".format(field.key, field.value))
                    # key = str(field.key)
                    json_form_data[str(field.key)] = str(field.value)
                    """#print(json.dumps(json_form_data))"""
                    genratedformfile = (
                        str(documentPathName)
                        + "/"
                        + str(documentFileName)
                        + str("_form")
                        + str(".txt")
                    )
                    s3resource.Object(s3BUCKET_NAME, genratedformfile).put(
                        Body=json.dumps(json_form_data)
                    )

        postgres_insert_query = """ UPDATE public.scannedimage set category = %s,lable = %s,jsonformdata=%s,detectedtext=%s where id = %s """
        record_to_update = (
            str(catagory_lst),
            str(lable_lst),
            json.dumps(json_form_data),
            str(detectedText),
            scannedimagelst_id,
        )
        cursor.execute(postgres_insert_query, record_to_update)
        connection.commit()
    return "success"


def setCategoryLable(
    dataset,
    detectedText,
    maxPercentage,
    page_refference_id,
    _casesequenceid,
    category,
    lable,
    _imagepath,
    _filename,
    scanorder,
    batchno,
    datasetid,
):
    status = ""
    indexSet = 0
    indexSetMatch = 0
    indexSetRatio = 0
    for i in dataset:
        indexSet = indexSet + 1
        # print(i)
        if detectedText.find(str(i.lower())) != -1:
            # print(i)
            indexSetMatch = indexSetMatch + 1

    if indexSet != 0:
        indexSetRatio = (indexSetMatch / indexSet) * 100

    if indexSetRatio >= maxPercentage:
        insert_table_scannedimagelist(
            page_refference_id,
            _casesequenceid,
            category,
            _imagepath,
            _filename,
            lable,
            scanorder,
            batchno,
            datasetid,
        )
        status = "Insert"

    return status


def insert_table_scannedimagelist(
    scannedimageuid,
    applicationid,
    category,
    imagepath,
    filename,
    lable,
    scanorder,
    batchno,
    datasetid,
):

    connection = psycopg2.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOSTNAME,
        port=DB_PORT,
        database=DB_DATABASE_NAME,
    )

    cursor = connection.cursor()
    postgres_insert_query = """ INSERT INTO public.scannedimagelist (id, scannedimageuid, applicationid, category, mimetype, imagepath, filename, lable, scanorder, active, ispermanent,batchno,datasetid)VALUES(uuid_generate_v4(), %s, %s, %s, 'image/png', %s, %s, %s, %s, true, false,%s,%s); """
    # print('#########################################################')
    # print(str(scannedimageuid))
    # print(str(applicationid))
    # print(str(category))
    # print(str(imagepath))
    # print(str(filename))
    # print(str(lable))
    # print(int(scanorder))
    # print(int(batchno))
    scannedimageuid_ = str(scannedimageuid)
    applicationid_ = str(applicationid)
    category_ = str(category)
    imagepath_ = str(imagepath)
    filename_ = str(filename)
    lable_ = str(lable)
    scanorder_ = int(scanorder)
    batchno_ = int(batchno)
    # print('#########################################################')
    record_to_update = (
        scannedimageuid_,
        applicationid_,
        category_,
        imagepath_,
        filename_,
        lable_,
        scanorder_,
        batchno_,
        datasetid,
    )
    cursor.execute(postgres_insert_query, record_to_update)
    connection.commit()
    return


def generate_table_csv(table_result, blocks_map, table_index):
    rows = get_rows_columns_map(table_result, blocks_map)

    table_id = "Table_" + str(table_index)

    # get cells.
    csv = ""  #'Table: {0}\n\n'.format(table_id)

    for row_index, cols in rows.items():

        for col_index, text in cols.items():
            csv += "{}".format(text) + ","
        csv += "\n"

    csv += "\n\n\n"
    return csv


def get_rows_columns_map(table_result, blocks_map):
    rows = {}
    for relationship in table_result["Relationships"]:
        if relationship["Type"] == "CHILD":
            for child_id in relationship["Ids"]:
                cell = blocks_map[child_id]
                if cell["BlockType"] == "CELL":
                    row_index = cell["RowIndex"]
                    col_index = cell["ColumnIndex"]
                    if row_index not in rows:
                        # create new row
                        rows[row_index] = {}

                    # get the text value
                    rows[row_index][col_index] = get_text(cell, blocks_map)
    return rows


def get_text(result, blocks_map):
    text = ""
    if "Relationships" in result:
        for relationship in result["Relationships"]:
            if relationship["Type"] == "CHILD":
                for child_id in relationship["Ids"]:
                    word = blocks_map[child_id]
                    if word["BlockType"] == "WORD":
                        text += word["Text"] + " "
                    if word["BlockType"] == "SELECTION_ELEMENT":
                        if word["SelectionStatus"] == "SELECTED":
                            text += "X "
    return text
