import csv
import json
from os import environ, stat

import psycopg2
from trp import Document

from awsutils import getJobResults, isJobComplete, startJob, textractAnalyzeDocument

s3BUCKET_NAME = environ.get("s3BUCKET_NAME")
DB_USERNAME = environ.get("DB_USERNAME")
DB_PASSWORD = environ.get("DB_PASSWORD")
DB_HOSTNAME = environ.get("DB_HOSTNAME")
DB_PORT = environ.get("DB_PORT")
DB_DATABASE_NAME = environ.get("DB_DATABASE_NAME")


def categoryImage(imageId):
    # key = imageId
    # print(DB_DATABASE_NAME)
    # print(DB_USERNAME)
    # print(DB_HOSTNAME)
    # print(DB_PORT)
    connection = psycopg2.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOSTNAME,
        port=DB_PORT,
        database=DB_DATABASE_NAME,
    )

    cursor = connection.cursor()

    getAllImageQuery = (
        "select d.s3bucketpathname, d.mime, d.documentpropertiesid from cjams.documentproperties d where d.documentpropertiesid = any('"
        + imageId
        + "'::uuid[]) order by d.documentdate asc"
    )
    cursor.execute(getAllImageQuery)
    getAllImageData = cursor.fetchall()
    print(getAllImageData)
    if len(getAllImageData) == 0:
        return

    deleteImageCategory = (
        "delete from public.imagecategory where document_id = any('"
        + imageId
        + "'::uuid[])"
    )
    cursor.execute(deleteImageCategory)
    connection.commit()

    insertImageCategory = (
        "INSERT INTO public.imagecategory (document_id) (select d.documentpropertiesid from cjams.documentproperties d where d.documentpropertiesid = any('"
        + imageId
        + "'::uuid[]))"
    )
    cursor.execute(insertImageCategory)
    connection.commit()

    for row in getAllImageData:
        s3BucketPath = row[0]
        mimeType = row[1]
        documentId = row[2]
        s3Path = s3BucketPath.split(
            "https://s3.us-east-1.amazonaws.com/" + s3BUCKET_NAME + "/"
        )[1]
        if s3Path == "":
            return

        updateCategoryStatus = (
            "update public.imagecategory set ocr_status = 'VALIDATION IN PROGRESS', updated_at = current_timestamp where document_id = '"
            + documentId
            + "'::uuid"
        )
        cursor.execute(updateCategoryStatus)
        connection.commit()

        detectedText = ""
        # print detected text
        if mimeType == "image/png" or mimeType == "image/jpeg":
            responseData = textractAnalyzeDocument(s3Path, ["TABLES"])
            for item in responseData["Blocks"]:
                if item["BlockType"] == "LINE":
                    detectedText += item["Text"] + "\n"
        elif mimeType == "application/pdf":
            jobId = startJob(s3Path)
            print("Started job with id: {}".format(jobId))
            if isJobComplete(jobId):
                responseData = getJobResults(jobId)
                for resultPage in responseData:
                    for item in resultPage["Blocks"]:
                        if item["BlockType"] == "LINE":
                            detectedText += item["Text"] + "\n"
        else:
            return
        # print(detectedText)
        catagory_lst = []
        lable_lst = []

        postgreSQL_ocrdataset_select_Query = "SELECT array_agg(keys) as keys, catagory, active, id, percentage, lable FROM public.ocrdataset where active = true group by catagory, active, id, percentage, lable order by id"
        cursor.execute(postgreSQL_ocrdataset_select_Query)
        postgreSQL_ocrdataset_select_Query_record = cursor.fetchall()

        current_page_catagory_lst = []

        if len(postgreSQL_ocrdataset_select_Query_record) == 0:
            return

        for row in postgreSQL_ocrdataset_select_Query_record:
            # print(row)
            ocrdataset_keys = row[0]
            ocrdataset_catagory = row[1]
            ocrdataset_active = row[2]
            ocrdataset_id = row[3]
            ocrdataset_percentage = row[4]
            ocrdataset_lable = row[5]

            if ocrdataset_catagory not in current_page_catagory_lst:
                status = setCategoryLable(
                    ocrdataset_keys,
                    str(detectedText.lower()),
                    ocrdataset_percentage,
                )
                if status == "Insert":
                    current_page_catagory_lst.append(ocrdataset_catagory)

        updateCategory = """update public.imagecategory set ocr_status = 'VALIDATION COMPLETED',category = %s, detected_text =%s, updated_at = current_timestamp where document_id = %s"""
        updateCategoryRecord = (
            str(current_page_catagory_lst),
            str(detectedText),
            documentId,
        )
        cursor.execute(updateCategory, updateCategoryRecord)
        connection.commit()

    connection.close()
    return "success"


def setCategoryLable(
    dataset,
    detectedText,
    maxPercentage,
):
    status = ""
    indexSet = 0
    indexSetMatch = 0
    indexSetRatio = 0
    for i in dataset:
        indexSet = indexSet + 1
        # print(i)
        if detectedText.find(str(i.lower())) != -1:
            # print(i)
            indexSetMatch = indexSetMatch + 1

    if indexSet != 0:
        indexSetRatio = (indexSetMatch / indexSet) * 100

    if indexSetRatio >= maxPercentage:
        status = "Insert"

    return status
