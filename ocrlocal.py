import json
from trp import Document
import csv
import psycopg2
from os import environ, stat
from awsutils import (
    textractAnalyzeDocument,
    startJob,
    isJobComplete,
    getJobResults,
    textractAnalyzeDocumentLocal,
)

s3BUCKET_NAME = environ.get("s3BUCKET_NAME")
DB_USERNAME = environ.get("DB_USERNAME")
DB_PASSWORD = environ.get("DB_PASSWORD")
DB_HOSTNAME = environ.get("DB_HOSTNAME")
DB_PORT = environ.get("DB_PORT")
DB_DATABASE_NAME = environ.get("DB_DATABASE_NAME")


def categoryImage():
    connection = psycopg2.connect(
        user=DB_USERNAME,
        password=DB_PASSWORD,
        host=DB_HOSTNAME,
        port=DB_PORT,
        database=DB_DATABASE_NAME,
    )

    cursor = connection.cursor()

    # s3Path = "test_document/Georgia DL1.jpeg"
    # s3Path = "test_document/Georgia DL2.jpeg"
    mimeType = "image/png"

    # s3Path = "test_document/Certificate of Occupancy.pdf"
    s3Path = "test_document/Completion_Certification.pdf"
    # s3Path = "test_document/Hospital_Permit_Application.pdf"
    # s3Path = "test_document/Identification_Affidavit.pdf"
    # s3Path = "test_document/Fire Safety Inspection.pdf"
    # s3Path = "test_document/Occupancy_Permit.pdf"
    # s3Path = "test_document/hhs-690.pdf"
    mimeType = "application/pdf"

    detectedText = ""
    # print detected text
    if mimeType == "image/png" or mimeType == "image/jpeg":
        responseData = textractAnalyzeDocument(s3Path, ["TABLES"])
        for item in responseData["Blocks"]:
            if item["BlockType"] == "LINE":
                detectedText += item["Text"] + "\n"
    elif mimeType == "application/pdf":
        jobId = startJob(s3Path)
        print("Started job with id: {}".format(jobId))
        if isJobComplete(jobId):
            responseData = getJobResults(jobId)
            for resultPage in responseData:
                for item in resultPage["Blocks"]:
                    if item["BlockType"] == "LINE":
                        detectedText += item["Text"] + "\n"
    else:
        return
    print(detectedText)
    catagory_lst = []
    lable_lst = []

    postgreSQL_ocrdataset_select_Query = "SELECT array_agg(keys) as keys, catagory, active, id, percentage, lable FROM public.ocrdataset where active = true group by catagory, active, id, percentage, lable order by id"
    cursor.execute(postgreSQL_ocrdataset_select_Query)
    postgreSQL_ocrdataset_select_Query_record = cursor.fetchall()

    current_page_catagory_lst = []

    if len(postgreSQL_ocrdataset_select_Query_record) == 0:
        return

    for row in postgreSQL_ocrdataset_select_Query_record:
        # print(row)
        ocrdataset_keys = row[0]
        ocrdataset_catagory = row[1]
        ocrdataset_active = row[2]
        ocrdataset_id = row[3]
        ocrdataset_percentage = row[4]
        ocrdataset_lable = row[5]

        if ocrdataset_catagory not in current_page_catagory_lst:
            status = setCategoryLable(
                ocrdataset_keys,
                str(detectedText.lower()),
                ocrdataset_percentage,
            )
            if status == "Insert":
                current_page_catagory_lst.append(ocrdataset_catagory)
    print(current_page_catagory_lst)
    connection.close()
    return current_page_catagory_lst


def setCategoryLable(
    dataset,
    detectedText,
    maxPercentage,
):
    status = ""
    indexSet = 0
    indexSetMatch = 0
    indexSetRatio = 0
    for i in dataset:
        indexSet = indexSet + 1
        # print(i)
        if detectedText.find(str(i.lower())) != -1:
            # print(i)
            indexSetMatch = indexSetMatch + 1

    if indexSet != 0:
        indexSetRatio = (indexSetMatch / indexSet) * 100

    if indexSetRatio >= maxPercentage:
        status = "Insert"

    return status
