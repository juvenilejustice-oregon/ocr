from os import environ
from pdf2image import convert_from_path, convert_from_bytes
import uuid
from awsutils import s3upload
import io

s3BUCKET_NAME = environ.get("s3BUCKET_NAME")


def pdf2img(pdfData):
    images = convert_from_path(
        "./Analysis of the Certificate of Need Application Submitted by MultiCare.pdf"
    )
    for i, image in enumerate(images):
        print("image count : ", i + 1)
        fileName = str(uuid.uuid4()) + ".jpg"
        image.save(fileName, "JPEG")
    return "success"


def pdfBinaryImage(binaryData, s3BucketName=s3BUCKET_NAME):
    images = convert_from_bytes(binaryData)
    imagePath = []
    for i, image in enumerate(images):
        uniqueFileName = uuid.uuid4()
        uploadPath = "testdocument/" + str(uniqueFileName) + ".jpg"
        # image.save(fileName, "JPEG")
        imagePath.append(uploadPath)
        img_byte_arr = io.BytesIO()
        image.save(img_byte_arr, format="JPEG")
        img_byte_arr = img_byte_arr.getvalue()
        s3upload(img_byte_arr, uploadPath, s3BucketName)
    return imagePath
